package exercicio1;

/*
* Crie uma classe java MaiorNumero que contenha um método que receba dois
* números inteiros e imprima o maior entre eles.
*
* Autor: Laércio Filho.
*
* */

public class MaiorNumero {

    public MaiorNumero() {
        // Imprime o retorno da comparação.
        System.out.println("O maior número é: " + retornaMaiorNumero(2,5));
    }

    /* Método recebe dois números por parâmetro e os compara
    * se num1 > num2 retorna num1, senão retorna num2 */
    private int retornaMaiorNumero(int num1, int num2) {
        if (num1 > num2) return num1;
        else return num2;
    }

    public static void main(String[] args) {
        new MaiorNumero();
    }
}
