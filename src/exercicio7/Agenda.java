package exercicio7;

/*
* Crie uma classe Contato que possui dois atributos: nome e email do tipo String (OK).
* Crie outra classe, chamada Agenda, que possui um atributo contatos do tipo
* vetor de Contato (OK). A classe Agenda deve conter um método para adicionar (OK) um
* novo contato em uma posição vazia do vetor (OK), outro método para buscar (OK) um
* contato (retorna uma instância de Contato) através do nome e, por fim, um
* método para excluir (OK) um contato através do nome.
*
* Autor: Laércio Filho.
*
* */

import java.util.ArrayList;

public class Agenda {
    private int tamanhoListaContatos = 0;
    private ArrayList<Contato> contatos = new ArrayList<>();

    public Agenda() {
        System.out.println("");
        adicionarNovoContato("Laercio", "teste@gmail.com");
        adicionarNovoContato("Gabriela", "teste@hotmail.com");
        System.out.println("");
        buscarContato("laercio");
        System.out.println("");
        removerContato("laercio");
        System.out.println("");
        buscarContato("laercio");
    }

    // Método para add um novo contato.
    private void adicionarNovoContato(String nome, String email) {
        if (tamanhoListaContatos < 100) {
            Contato contato = new Contato(nome,email);
            this.contatos.add(contato);
            this.tamanhoListaContatos++;
            System.out.println("Contato adicionado com sucesso.");
        } else System.out.println("Ocorreu um problema: Agenda cheia.");
    }

    // Método para localizar os dados de um contato.
    private void buscarContato(String nome) {
        boolean flagContatoLocalizado = false;
        boolean flagTituloContatoLocalizado = false;
        if (!this.contatos.isEmpty()) {
            for (int i = 0; i < this.contatos.size(); i++) {
                if (this.contatos.get(i).getNome().equalsIgnoreCase(nome)) {
                    if (!flagTituloContatoLocalizado) {
                        System.out.println("Contato localizado:");
                        flagTituloContatoLocalizado = true;
                    }
                    System.out.println("Nome: " + this.contatos.get(i).getNome() + " - " + "E-mail: " + this.contatos.get(i).getEmail());
                    flagContatoLocalizado = true;
                }
            }
            if (!flagContatoLocalizado) System.out.println("Contato não localizado.");
        } else System.out.println("Não há contatos na agenda.");
    }

    // Método para remover um contato.
    private void removerContato(String nome) {
        boolean flagContatoRemovido = false;
        if (!this.contatos.isEmpty()) {
            for (int i = 0; i < this.contatos.size(); i++) {
                if (this.contatos.get(i).getNome().equalsIgnoreCase(nome)) {
                    this.contatos.remove(i);
                    this.tamanhoListaContatos--;
                    System.out.println("Contato removido com sucesso.");
                    flagContatoRemovido = true;
                }
            }
            if (!flagContatoRemovido) System.out.println("Contato não localizado.");
        } else System.out.println("Não há contatos na agenda.");
    }

    public static void main(String[] args) {
        new Agenda();
    }
}
