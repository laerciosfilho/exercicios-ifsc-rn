package exercicio2;

/*
* Crie uma classe java NumeroDecrescente que contenha um método que receba
* um número inteiro e imprima, em ordem decrescente, o valor do número até
* 0.
*
* Autor: Laércio Filho.
*
* */

public class NumeroDecrescente {

    public NumeroDecrescente() {
        // Método recebe um número inteiro.
        imprimeEmOrdemDecrescente(6);
    }

    private void imprimeEmOrdemDecrescente(int num) {
        // Loop imprime em ordem decrescente até 0.
        for (int i = num; i >= 0; i--) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        new NumeroDecrescente();
    }
}
