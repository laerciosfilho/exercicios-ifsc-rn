package exercicio4;

/*
* Crie uma classe java TrocaNumero que contenha um método que receba dois
* números NumA e NumB, nessa ordem, e imprima em ordem inversa, isto é, se os
* dados lidos forem NumA = 5 e NumB = 9, por exemplo, devem ser impressos na
* ordem NumA = 9 e NumB = 5.
*
* Autor: Laércio Filho.
*
* */

public class TrocaNumero {

    public TrocaNumero() {
        imprimeOrdemInversa(5,9);
    }

    private void imprimeOrdemInversa(int num1, int num2) {
        int num1_inverso = num2;
        int num2_inverso = num1;
        System.out.println("Número 1 = " + num1_inverso + " e Número 2 = " + num2_inverso);
    }

    public static void main(String[] args) {
        new TrocaNumero();
    }
}
