package exercicio6;

/*
* Crie uma classe MediaAluno que contenha um atributo do tipo vetor de
* inteiros com o nome de notas. Essa classe deve ter um método para adicionar
* as notas nesse vetor (os valores que podem ser adicionados no vetor são os
* inteiros entre 0 e 100, caso contrário imprime uma mensagem de erro e não
* adiciona) e outro método que calcule a média de um aluno e imprima essa
* média.
*
* Autor: Laércio Filho.
*
* */

public class MediaAluno {
    private int aux = 0;
    private int numNota = 1;
    private int qntNotas = 3;
    private int[] notas = new int[this.qntNotas];

    public MediaAluno() {
        adicionaNotas(100);
        adicionaNotas(100);
        adicionaNotas(101);
        calculaMedia();
    }

    private void adicionaNotas(int nota) {
        if (nota >= 0 && nota <= 100) {
            this.notas[this.aux] = nota;
            System.out.println("Nota " + this.numNota + " adicionada na posição " + this.aux);
            this.numNota++;
            this.aux++;
        } else System.out.println("Não foi possível adicionar a nota " + this.numNota + ". O valor " + nota + " é inválido!");
    }

    private void calculaMedia() {
        int media = 0;
        for (int i = 0; i < this.aux; i++) {
            media += this.notas[i];
        }
        media = (media / this.qntNotas);
        System.out.println("A média do aluno é: " + media);
    }

    public static void main(String[] args) {
        new MediaAluno();
    }
}
