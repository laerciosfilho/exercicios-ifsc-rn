package exercicio3;

/*
* Escreva um programa que imprima na tela a soma dos números ímpares entre
* 0 e 30 e a multiplicação dos números pares entre 0 e 30.
*
*  Autor: Laércio Filho.
*/

public class SomaNumerosImpares {

    public SomaNumerosImpares() {
        imprimeSomaDosNumerosImpares();
    }

    private void imprimeSomaDosNumerosImpares() {
        int somaImpares = 0;
        int multiplicacaoPares = 1;
        for (int i = 0; i <= 30 ; i++) {
            if (i % 2 == 1) somaImpares += i;
            else multiplicacaoPares = multiplicacaoPares * i;

        }
        System.out.println("A soma dos números ímpares de 0 à 30 é: " + somaImpares + "\n" +
                "E a multiplicação dos números pares é: " + multiplicacaoPares);
    }

    public static void main(String[] args) {
        new SomaNumerosImpares();
    }
}
