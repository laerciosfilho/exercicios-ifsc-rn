package exercicio5;

/*
*
* Crie uma classe java ComparaNumero que contenha um método que receba
* dois números e indique se são iguais ou se são diferentes. Mostre o maior e o
* menor (nesta sequência).
*
* Autor: Laércio Filho.
*
* */

public class ComparaNumero {

    public ComparaNumero() {
        comparaNumeros(200,200);
    }

    private void comparaNumeros(int num1, int num2) {
        if (num1 == num2) System.out.println(num1 + " (número 1) e " + num2 + " (número 2) são iguais.");
        else {
            if (num1 > num2) System.out.println(num1 + " (número 1) e " + num2 + " (número 2) são diferentes.\n" +
                    "Sendo que o maior é " + num1 + ", e o menor é " + num2 + ".");
            else System.out.println(num1 + " (número 1) e " + num2 + " (número 2) são diferentes.\n" +
                    "Sendo que o maior é " + num2 + ", e o menor é " + num1 + ".");
        }
    }

    public static void main(String[] args) {
        new ComparaNumero();
    }
}
